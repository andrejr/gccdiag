set(
  sources
  src/compilation_database.cpp
  src/config.cpp
  src/json_utils.cpp
  src/split_string.cpp
)

set(
  exe_sources
  src/main.cpp
  ${sources}
)

set(
  headers
  # include/gccdiag/tmp.hpp
)
