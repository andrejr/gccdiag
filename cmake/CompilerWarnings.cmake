# from here:
#
# https://github.com/lefticus/cppbestpractices/blob/master/02-Use_the_Tools_Available.md
# Courtesy of Jason Turner

function(set_project_warnings project_name)
  set(
    MSVC_WARNINGS
    # Baseline reasonable warnings
    /W4
    # 'identifier': conversion from 'type1' to 'type1', possible loss of data
    /w14242
    # 'operator': conversion from 'type1:field_bits' to 'type2:field_bits',
    # possible loss of data
    /w14254
    # 'function': member function does not override any base class virtual
    # member function
    /w14263
    # 'classname': class has virtual functions, but destructor is not virtual
    # instances of this class may not be destructed correctly
    /w14265
    # 'operator': unsigned/negative constant mismatch
    /w14287
    # nonstandard extension used: 'variable': loop control variable declared in
    # the for-loop is used outside the for-loop scope
    /we4289
    # 'operator': expression is always 'boolean_value'
    /w14296
    # 'variable': pointer truncation from 'type1' to 'type2'
    /w14311
    # expression before comma evaluates to a function which is missing an
    # argument list
    /w14545
    # function call before comma missing argument list
    /w14546
    # 'operator': operator before comma has no effect; expected operator with
    # side-effect
    /w14547
    # 'operator': operator before comma has no effect; did you intend
    # 'operator'?
    /w14549
    # expression has no effect; expected expression with side- effect
    /w14555
    # pragma warning: there is no warning number 'number'
    /w14619
    # Enable warning on thread un-safe static member initialization
    /w14640
    # Conversion from 'type1' to 'type_2' is sign-extended. This may cause
    # unexpected runtime behavior.
    /w14826
    # wide string literal cast to 'LPSTR'
    /w14905
    # string literal cast to 'LPWSTR'
    /w14906
    # illegal copy-initialization; more than one user-defined conversion has
    # been implicitly applied
    /w14928
    # standards conformance mode for MSVC compiler.
    /permissive-
  )

  set(
    CLANG_WARNINGS
    -Wall
    # reasonable and standard
    -Wextra
    # warn the user if a variable declaration shadows one from a parent context
    -Wshadow
    # warn the user if a class with virtual functions has a non-virtual
    # destructor. This helps catch hard to track down memory errors
    -Wnon-virtual-dtor
    # warn for c-style casts
    -Wold-style-cast
    # warn for potential performance problem casts
    -Wcast-align
    # warn on anything being unused
    -Wunused
    # warn if an undefined identifier is evaluated in an #if directive.
    # Such identifiers are replaced with zero.
    -Wundef
    # warn if you overload (not override) a virtual function
    -Woverloaded-virtual
    # warn if non-standard C++ is used
    -Wpedantic
    # warn on type conversions that may lose data
    -Wconversion
    # warn on sign conversions
    -Wsign-conversion
    # warn if a null dereference is detected
    -Wnull-dereference
    # warn if float is implicit promoted to double
    -Wdouble-promotion
    # warn on security issues around functions that format output (ie printf)
    -Wformat=2
    # allow unused function parameters
    -Wno-unused-parameter
    # allow unknown pragmas
    -Wno-unknown-pragmas
  )

  if(${PROJECT_NAME}_WARNINGS_AS_ERRORS)
    set(CLANG_WARNINGS ${CLANG_WARNINGS} -Werror)
    set(MSVC_WARNINGS ${MSVC_WARNINGS} /WX)
  endif()

  set(
    GCC_WARNINGS
    ${CLANG_WARNINGS}
    # warn if indentation implies blocks where blocks do not exist
    -Wmisleading-indentation
    # warn if if / else chain has duplicated conditions
    -Wduplicated-cond
    # warn if if / else branches have duplicated code
    -Wduplicated-branches
    # warn about logical operations being used where bitwise were probably
    # wanted
    -Wlogical-op
    # warn if you perform a cast to the same type
    -Wuseless-cast
    # warn on float equality issues
    -Wfloat-equal
    # Valid only for C:
    # warn about duplicated condition in if-else-if chains
    # warn if a goto statement or a switch statement jumps forward across the
    # initialization of a variable, or jumps backward to a label after the
    # variable has been initialized.
    # -Wjump-misses-init
  )

  if(MSVC)
    set(PROJECT_WARNINGS ${MSVC_WARNINGS})
  elseif(CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
    set(PROJECT_WARNINGS ${CLANG_WARNINGS})
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(PROJECT_WARNINGS ${GCC_WARNINGS})
  else()
    message(AUTHOR_WARNING "No compiler warnings set for
    '${CMAKE_CXX_COMPILER_ID}' compiler.")
  endif()

  if(${PROJECT_NAME}_BUILD_HEADERS_ONLY)
    target_compile_options(${project_name} INTERFACE ${PROJECT_WARNINGS})
  else()
    target_compile_options(${project_name} PUBLIC ${PROJECT_WARNINGS})
  endif()

  if(NOT TARGET ${project_name})
    message(AUTHOR_WARNING "${project_name} is not a target, thus no compiler
    warning were added.")
  endif()
endfunction()
