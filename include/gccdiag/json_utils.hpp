#pragma once

#include <filesystem>
#include <optional>
#include <vector>

#include <boost/json.hpp>

using std::optional;
using std::string;
using std::vector;

namespace fs = std::filesystem;
using fs::path;
namespace json = boost::json;

namespace gccdiag {
json::value json_value_from_file(const path &file_path);

optional<vector<string>>
get_string_vector_from_json_array(const json::object &obj,
                                  const json::string_view &key);

optional<vector<string>>
get_string_vector_from_json_string(const json::object &obj,
                                   const json::string_view &key);

optional<path> get_path_from_json(const json::object &obj,
                                  const json::string_view &key);

vector<string>
get_string_vector_from_json_array_throw(const json::object &obj,
                                        const json::string_view &key);

path get_path_from_json_throw(const json::object &obj,
                              const json::string_view &key);

vector<string>
get_string_vector_from_json_string_throw(const json::object &obj,
                                         const json::string_view &key);
} // namespace gccdiag
