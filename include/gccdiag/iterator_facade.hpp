#pragma once

#include <algorithm>

namespace util::iterator {
template <class Reference>
struct arrow_proxy {
    Reference r;
    Reference *operator->() { return &r; }
};

template <typename T>
concept impls_distance_to = requires(const T it)
{
    it.distance_to(it);
};

// Base case
template <typename>
struct infer_difference_type {
    using type = std::ptrdiff_t;
};

// Case when `T` provides a `distance_to`
template <impls_distance_to T>
struct infer_difference_type<T> {
    static const T &it;
    using type = decltype(it.distance_to(it));
};

template <typename T>
using infer_difference_type_t = typename infer_difference_type<T>::type;

// Check for .increment
template <typename T>
concept impls_increment = requires(T t)
{
    t.increment();
};

// Check for .decrement
template <typename T>
concept impls_decrement = requires(T t)
{
    t.decrement();
};

// Check for .advance
template <typename T>
concept impls_advance = requires(T it, const infer_difference_type_t<T> offset)
{
    it.advance(offset);
};

template <typename Arg, typename Iter>
concept difference_type_arg =
    std::convertible_to<Arg, infer_difference_type_t<Iter>>;

template <typename T>
struct infer_value_type {
    static const T &_it;
    using type = std::remove_cvref_t<decltype(*_it)>;
};

template <typename T>
requires requires { typename T::value_type; }
struct infer_value_type<T> {
    using type = typename T::value_type;
};

template <typename T>
using infer_value_type_t = typename infer_value_type<T>::type;

// Check for .equal_to
template <typename T>
concept impls_equal_to = requires(const T it)
{
    {
        it.equal_to(it)
        } -> std::same_as<bool>;
};

// We can meet "random access" if it provides
// both .advance() and .distance_to()
template <typename T>
concept meets_random_access = impls_advance<T> && impls_distance_to<T>;

// We meet `bidirectional` if we are random_access, OR we have .decrement()
template <typename T>
concept meets_bidirectional = meets_random_access<T> || impls_decrement<T>;

// Detect if the iterator declares itself to be a single-pass iterator.
// (More on this later.)
template <typename T>
concept decls_single_pass = bool(T::single_pass_iterator);

template <typename T, typename Addend>
concept can_add = requires(T t, Addend a)
{
    t += a;
};

template <typename Iter, typename T>
concept iter_sentinel_arg = std::same_as<T, typename T::sentinel_type>;

template <typename T>
concept impls_at_end = requires(T t)
{
    t.at_end();
};

template <typename Iter, typename T>
concept impls_sentinel = iter_sentinel_arg<Iter, T> && impls_at_end<Iter>;

template <typename Derived>
class iterator_facade {
public:
    using self_type = Derived;

    decltype(auto) operator*() const { return _self().dereference(); }

    auto operator->() const
    {
        decltype(auto) ref = **this;
        if constexpr (std::is_reference_v<decltype(ref)>) {
            // `ref` is a true reference, and we're safe to take its address
            return std::addressof(ref);
        } else {
            // `ref` is *not* a reference. Returning its address would be the
            // address of a local. Return that thing wrapped in an arrow_proxy.
            return arrow_proxy(std::move(ref));
        }
    }

    self_type &operator++()
    {
        if constexpr (impls_increment<self_type>) {
            // Prefer .increment() if available
            _self().increment();
        } else {
            static_assert(impls_advance<self_type>,
                          "Iterator subclass must provide either "
                          ".advance() or .increment()");
            _self().advance(1);
        }
        return _self();
    }

    self_type operator++(int)
    {
        auto copy = _self();
        ++*this;
        return copy;
    }

    self_type &operator--()
    {
        if constexpr (impls_decrement<self_type>) {
            // Prefer .decrement() if available
            _self().decrement();
        } else {
            static_assert(impls_advance<self_type>,
                          "Iterator subclass must provide either "
                          ".advance() or .decrement()");
            _self().advance(-1);
        }
        return _self();
    }

    // Postfix:
    self_type operator--(int) requires impls_decrement<self_type>
    {
        auto copy = *this;
        --*this;
        return copy;
    }

    friend bool operator==(const self_type &left, const self_type &right)
    {
        if constexpr (impls_equal_to<self_type>) {
            return left.equal_to(right);
        } else {
            static_assert(impls_distance_to<self_type>,
                          "Iterator must provide `.equal_to()` "
                          "or `.distance_to()`");
            return left.distance_to(right) == 0;
        }
    }

    friend bool
    operator==(const self_type &self,
               iter_sentinel_arg<self_type> auto /*unused*/) requires
        impls_at_end<self_type>
    {
        return self.at_end();
    }

    friend self_type
    operator+(self_type left, difference_type_arg<self_type> auto off) requires
        impls_advance<self_type>
    {
        return left += off;
    }

    friend self_type
    operator+(difference_type_arg<self_type> auto off,
              self_type right) requires impls_advance<self_type>
    {
        return right += off;
    }

    friend self_type &
    operator-=(self_type &left,
               difference_type_arg<self_type> auto off) requires
        impls_advance<self_type>
    {
        return left = left - off;
    }

    friend self_type
    operator-(self_type left, difference_type_arg<self_type> auto off) requires
        impls_advance<self_type>
    {
        return left + -off;
    }

    friend self_type &
    operator-(const self_type &left,
              const self_type &right) requires impls_distance_to<self_type>
    {
        // Many many times must we `++right` to reach `left` ?
        return right.distance_to(left);
    }

    decltype(auto) operator[](difference_type_arg<self_type> auto off) requires
        impls_advance<self_type>
    {
        return *(_self() + off);
    }

    friend auto
    operator<=>(const self_type &left,
                const self_type &right) requires impls_distance_to<self_type>
    {
        return (left - right) <=> 0;
    }

protected : self_type &_self() { return static_cast<self_type &>(*this); }
    const self_type &_self() const
    {
        return static_cast<const self_type &>(*this);
    }

    friend self_type &
    operator+=(self_type &self,
               difference_type_arg<self_type> auto offset) requires
        impls_advance<self_type>
    {
        self.advance(offset);
        return self;
    }
};

} // namespace util::iterator

namespace std {
template <typename Iter>
requires std::is_base_of_v<util::iterator::iterator_facade<Iter>, Iter>
struct iterator_traits<Iter> {
    static const Iter &_it;
    using reference = decltype(*_it);
    using pointer = decltype(_it.operator->());
    using value_type = util::iterator::infer_value_type_t<Iter>;
    using difference_type = util::iterator::infer_difference_type_t<Iter>;
    using iterator_category = conditional_t<
        util::iterator::meets_random_access<Iter>,
        // We meet the requirements of random-access:
        random_access_iterator_tag,
        // We don't:
        conditional_t<util::iterator::meets_bidirectional<Iter>,
                      // We meet requirements for bidirectional usage:
                      bidirectional_iterator_tag,
                      // We don't:
                      conditional_t<util::iterator::decls_single_pass<Iter>,
                                    // A single-pass iterator is an
                                    // input-iterator:
                                    input_iterator_tag,
                                    // Otherwise we are a forward iterator:
                                    forward_iterator_tag>>>;

    // Just set this to the iterator_category, for now
    using iterator_concept = iterator_category;
};

} // namespace std
