#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

namespace gccdiag::util {
vector<string> split_string(const string &text);
}
