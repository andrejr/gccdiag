#pragma once

#include <iterator>
#include <optional>
#include <ostream>
#include <string>
#include <vector>

namespace gccdiag::util {
template <typename T>
inline std::ostream &operator<<(std::ostream &out, const std::vector<T> &v)
{
    out << "[";
    if (!v.empty()) {
        std::copy(v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
        out << "\b\b]";
    } else {
        out << "]";
    }
    return out;
}

template <>
inline std::ostream &operator<<(std::ostream &out,
                                const std::vector<std::string> &v)
{
    out << "[";
    if (!v.empty()) {
        for (auto const &s : v) {
            out << "\"" << s << "\", ";
        }
        out << "\b\b]";
    } else {
        out << "]";
    }
    return out;
}

template <typename T>
inline std::ostream &operator<<(std::ostream &out, const std::optional<T> &o)
{
    if (o) {
        out << *o;
    } else {
        out << "{}";
    }
    return out;
}

} // namespace gccdiag::util
