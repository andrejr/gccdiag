#pragma once
#include <cstdlib>
#include <filesystem>
#include <stdexcept>
#include <string>

namespace fs = std::filesystem;
using fs::path;

inline path get_config_dir(const std::string &program_name)
{
    {
#ifdef __APPLE__
        if (const char *home = std::getenv("HOME")) {
            return path(home) / "Library/Preferences" / program_name;
        }
#elif defined _WIN32 || defined _WIN64
        if (const char *appdata = std::getenv("APPDATA")) {
            return path(appdata) / program_name;
        }
#else // Linux
        if (const char *xdg_conf = std::getenv("XDG_CONFIG_HOME")) {
            return path(xdg_conf) / program_name;
        }
        if (const char *home = std::getenv("HOME")) {
            return path(home) / ".config" / program_name;
        }
#endif
        throw std::runtime_error("Couldn't find home directory.");
    }
}
