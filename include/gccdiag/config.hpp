#pragma once

#include "gccdiag/json_utils.hpp"

#include <filesystem>
#include <optional>

#include <boost/json.hpp>

using std::optional;
using std::string;
using std::vector;

namespace fs = std::filesystem;
using fs::path;
namespace json = boost::json;

namespace gccdiag {
class Config {
    optional<path> compiler_path_;
    optional<vector<string>> additional_args_;
    optional<vector<string>> default_args_;
    optional<vector<string>> args_to_ignore_;

public:
    Config() = default;
    Config(optional<path> compiler_path,
           optional<vector<string>> additional_args,
           optional<vector<string>> default_args,
           optional<vector<string>> args_to_ignore)
        : compiler_path_(std::move(compiler_path)),
          additional_args_(std::move(additional_args)),
          default_args_(std::move(default_args)),
          args_to_ignore_(std::move(args_to_ignore))
    {
    }

    void update(const Config &other)
    {
        if (other.compiler_path_) {
            compiler_path_ = other.compiler_path_;
        }
        if (other.additional_args_) {
            additional_args_ = other.additional_args_;
        }
        if (other.default_args_) {
            default_args_ = other.default_args_;
        }
        if (other.args_to_ignore_) {
            args_to_ignore_ = other.args_to_ignore_;
        }
    }

    [[nodiscard]] const auto &compiler_path() const { return compiler_path_; }
    [[nodiscard]] const auto &additional_args() const
    {
        return additional_args_;
    }
    [[nodiscard]] const auto &default_args() const { return default_args_; }
    [[nodiscard]] const auto &args_to_ignore() const
    {
        return args_to_ignore_;
    }

    static Config defaults()
    {
        return Config("", std::vector<string> {},
                      std::vector<string> { "-S", "-x", "c" },
                      std::vector<string> {});
    }
};

Config tag_invoke(json::value_to_tag<Config> /*unused*/,
                  json::value const &jv);

Config config_from_file(const path &file);

std::ostream &operator<<(std::ostream &os, Config const &c);

} // namespace gccdiag
