#pragma once
#include "gccdiag/iterator_facade.hpp"

#include <filesystem>
#include <iostream>
#include <string>
#include <utility>

namespace fs = std::filesystem;
using fs::path;

namespace util::iterator {
const path root_path = path { "/" };

class path_ancestor_iterator
    : public util::iterator::iterator_facade<path_ancestor_iterator> {
    path current_level_;

public:
    path_ancestor_iterator() = default;
    explicit path_ancestor_iterator(path path)
        : current_level_(std::move(path))
    {
    }

    auto begin() const { return *this; }
    auto end() const { return path_ancestor_iterator(path {}); }

    void increment()
    {
        current_level_ =
            (current_level_.has_parent_path() && current_level_ != root_path)
            ? current_level_.parent_path()
            : path {};
    }

    const path &dereference() const { return current_level_; }

    [[nodiscard]] bool equal_to(const path_ancestor_iterator &other) const
    {
        return current_level_ == other.current_level_;
    }
};

class path_ancestor_range {
    path path_;

public:
    path_ancestor_range() = default;
    explicit path_ancestor_range(path path) : path_(std::move(path)) { }
    auto begin() const { return path_ancestor_iterator { path_ }; }
    auto end() const { return path_ancestor_iterator { path_ }.end(); }
};
} // namespace util::iterator
