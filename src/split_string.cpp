#include "gccdiag/split_string.hpp"

#include <iterator>
#include <sstream>

namespace gccdiag::util {
vector<string> split_string(const string &text)
{
    std::istringstream iss(text);
    return vector<string>(std::istream_iterator<string> { iss },
                          std::istream_iterator<string> {});
}
} // namespace gccdiag::util
