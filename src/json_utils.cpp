#include "gccdiag/json_utils.hpp"

#include "gccdiag/split_string.hpp"

#include <iostream>

#include <boost/filesystem/fstream.hpp>
#include <boost/range/algorithm.hpp>

namespace gccdiag {

class key_error : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};
using namespace std::string_literals;

json::value json_value_from_file(const path &file_path)
{
    try {
        json::stream_parser p;
        std::ifstream file;
        try {
            file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            file.open(file_path);
            file.exceptions(std::ifstream::badbit);
            constexpr size_t buffer_size = 4096;
            do {
                std::array<char, buffer_size> buf {};
                file.read(buf.data(), buf.size());
                auto const nread = file.gcount();
                p.write(buf.data(), static_cast<size_t>(nread));
            } while (!file.eof());
        } catch (const std::ifstream::failure &e) {
            throw std::runtime_error("Unable to open/read file");
        }
        if (!p.done()) {
            throw std::runtime_error("Incomplete JSON file");
        }
        auto jv = p.release();
        return jv;
    } catch (...) {
        std::cerr << "Error occurred while parsing JSON file " << file_path
                  << ":\n";
        throw;
    }
}

optional<path> get_path_from_json(const json::object &obj,
                                  const json::string_view &key)
{
    try {
        if (obj.contains(key)) {
            return path(string(obj.at(key).as_string().subview()));
        }
    } catch (std::exception &e) {
        throw std::runtime_error("While parsing key \""s + std::string(key)
                                 + "\":\n"s + e.what());
    }
    return {};
}

optional<vector<string>>
get_string_vector_from_json_array(const json::object &obj,
                                  const json::string_view &key)
{
    try {
        if (obj.contains(key)) {
            auto arr = obj.at(key).as_array();
            vector<string> res;
            res.reserve(arr.size());
            boost::transform(arr, std::back_inserter(res), [](const auto &s) {
                return string(s.as_string().subview());
            });
            return res;
        }
    } catch (std::exception &e) {
        throw std::runtime_error("While parsing key \""s + std::string(key)
                                 + "\":\n"s + e.what());
    }
    return {};
}

optional<vector<string>>
get_string_vector_from_json_string(const json::object &obj,
                                   const json::string_view &key)
{
    try {
        if (obj.contains(key)) {
            auto s = string(obj.at(key).as_string().subview());
            return util::split_string(s);
        }
    } catch (std::exception &e) {
        throw std::runtime_error("While parsing key \""s + std::string(key)
                                 + "\":\n"s + e.what());
    }
    return {};
}

vector<string>
get_string_vector_from_json_array_throw(const json::object &obj,
                                        const json::string_view &key)
{
    auto res = get_string_vector_from_json_array(obj, key);
    if (res) {
        return *res;
    }
    throw key_error("Key not found: \"" + std::string(key) + "\"");
}

vector<string>
get_string_vector_from_json_string_throw(const json::object &obj,
                                         const json::string_view &key)
{
    auto res = get_string_vector_from_json_string(obj, key);
    if (res) {
        return *res;
    }
    throw key_error("Key not found: \"" + std::string(key) + "\"");
}

path get_path_from_json_throw(const json::object &obj,
                              const json::string_view &key)
{
    auto res = get_path_from_json(obj, key);
    if (res) {
        return *res;
    }

    throw key_error("Key not found: \"" + std::string(key) + "\"");
}

} // namespace gccdiag
