#pragma once

#include <stdexcept>

namespace gccdiag {

class path_error : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

}
