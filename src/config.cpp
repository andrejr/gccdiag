#include "gccdiag/config.hpp"

#include "gccdiag/json_utils.hpp"
#include "gccdiag/vector_print.hpp"

#include <iostream>
#include <optional>

#include <boost/filesystem/fstream.hpp>
#include <boost/json.hpp>
#include <boost/range/algorithm.hpp>

namespace gccdiag {
Config tag_invoke(json::value_to_tag<Config> /*unused*/, json::value const &jv)
{
    json::object const &obj = jv.as_object();
    return Config { get_path_from_json(obj, "compiler_path"),
                    get_string_vector_from_json_array(obj, "additional_args"),
                    get_string_vector_from_json_array(obj, "default_args"),
                    get_string_vector_from_json_array(obj, "args_to_ignore") };
}

Config config_from_file(const path &file_path)
{
    auto jv = json_value_from_file(file_path);
    return json::value_to<Config>(jv);
}

std::ostream &operator<<(std::ostream &os, Config const &c)
{
    using namespace gccdiag::util;
    os << "{ compiler_path: " << c.compiler_path() << ", "
       << "additional_args: " << c.additional_args() << ", "
       << "default_args: " << c.default_args() << ", "
       << "args_to_ignore: " << c.args_to_ignore() << " }";
    return os;
}

} // namespace gccdiag
