# `gccdiag`

[![pipeline status](https://gitlab.com/andrejr/gccdiag/badges/master/pipeline.svg)](https://gitlab.com/andrejr/gccdiag/-/commits/master)

A utility to get `gcc` (`avr-gcc`, `gcc-arm-none-eabi`, …, or other compiler)
diagnostics for a source file, with appropriate compiler flags extracted from a
compilation database (`compile_commands.json`).

Here it is used in [Neovim](https://github.com/neovim/neovim), with
[diagnostic-languageserver](https://github.com/iamcco/diagnostic-languageserver)
universal language server for linters/formatters:  
![preview](doc/preview.png)

## Motivation

`gccdiag`'s main use case is to provide `gcc` diagnostics for your text editor.
It does so with the help of a universal language server, like
[efm-langserver](https://github.com/mattn/efm-langserver) or
[`diagnostic-langserver`](https://github.com/iamcco/diagnostic-languageserver).

While you can always use [`clangd`](https://clangd.llvm.org/) language server
to get `clang` compiler diagnostics while editing a file, `gcc` compiler
warnings are often superior, `gcc` has more warning flags available and better
explanations, as well as better `c++2x` support.

## Is this actually useful?

Yes, if you enable useful compiler warnings.
Here are some recommendations:

- [The Best and Worst GCC Compiler Flags For Embedded](https://interrupt.memfault.com/blog/best-and-worst-gcc-clang-compiler-flags) (not just for embedded)
- [C++ Best Practices: Use The Tools Available](https://lefticus.gitbooks.io/cpp-best-practices/content/02-Use_the_Tools_Available.html)
- [Useful GCC warning options not enabled by -Wall -Wextra](https://kristerw.blogspot.com/2017/09/useful-gcc-warning-options-not-enabled.html)

Or you may simply look at warnings [this project uses](cmake/CompilerWarnings.cmake).

## How it works

For the supplied argument (C/C++ source path), gccdiag searches the
`compile_commands.json` file in the current (or some parent) directory to
obtain the compilation commandline arguments for it.
For header files, it finds the source file with the same name, preferably in
the same folder (but any subfolder will do otherwise), and gets the commandline
args for it.
It then replaces the `-o /output/path.o` of that commandline with
`-o /dev/null`, so the output is thrown out, and we only get stdout/stderr
output.
This is better than using the `-fsyntax-only` flag, because a full compilation
catches more warnings.

If the `-c <path>`/`--compiler=<path>` argument is supplied, the compiler
executable in the commandline is replaced with its value.
The compiler is then run with the prepared arguments.

The tool can also add additional arguments to every commandline, and specify
default arguments if the file isn't found in the compilation database. These
values can also be set in a config file, .gccdiag or \_gccdiag, as explained in
the Usage section.

## Installation

On Arch Linux, you can get it from the AUR: [gccdiag](https://aur.archlinux.org/packages/gccdiag/).

Otherwise, you have to build it yourself.
`CMake>=3.19` is a prerequisite, and the CMakeLists also use
[Conan](https://conan.io/) by default to obtain Boost and
[Taywee's args](https://github.com/Taywee/args).
You can disable Conan altogether if you have Boost and Args installed
system-wide (add `-Dgccdiag_USE_CONAN=off` to the first CMake command), or you
can disable it just for Boost if you have a recent Boost lib (>=1.76.0) but not
Args (add `-Dgccdiag_USE_CONAN_BOOST=off`).

To build the project run:

```
cmake -B build -S. && cmake --build build
```

in the repo folder.

To install, run:

```
make -C build install
```

You'll probably have to prefix that with sudo to install the package
system-wide.

## Usage

You need to have a `compile_commands.json` file/symlink in your program's root
directory.
You can either enable generating them in CMake
(`set(CMAKE_EXPORT_COMPILE_COMMANDS ON)`, after `project(...)`), or use
[Bear](https://github.com/rizsotto/Bear) to generate them if you're using plain
Make.

Here's a quick reference of flags (`this is the output of gccdiag --help`):

```
gccdiag source {OPTIONS}

  Run `gcc` (or other compiler) on a source file to obtain diagnostics, with
  compile flags from `compile_commands.json`.

OPTIONS:

    source                            Path to file
    -a[args], --add-args=[args]       Compiler args to add when calling (empty
                                      by default)
    -c[path], --compiler=[path]       Compiler path (empty by default). If
                                      empty, the compiler will be inferred
                                      from the compilation database
    -d[args], --default-args=[args]   Default args to use if the file isn't
                                      found in `compile_commands.json`; if
                                      empty, the compiler isn't invoked (empty
                                      by default; "-S -x c" or "-S -x c++" are
                                      reasonable alternatives, but will yield
                                      some false positives)
    -i[args], --ignore-args=[args]    Compiler args to omit when calling
    -h, --help                        Prints help information
    -v, --version                     Prints the version number and exits
    "--" can be used to terminate flag options and force all following
    arguments to be treated as positional options

  The program searches the current folder folders above it in the hierarchy
  for a "compile_commands.json" file, until it finds the first match. This
  file is then parsed as the compilation database.
  The program can also be configured by reading a ".gccdiag" or "_gccdiag"
  file in the current working directory or above, as well as the global one in
  $XDG_CONFIG_HOME/gccdiag/gccdiag. This file contains a JSON dict with up to
  three keys: "additional_args", "default_args", "args_to_ignore", which
  correspond to flags listed above.
```

How you call it it depends on which editor you use.
You may use `gccdiag` with one of the universal language servers or with a
plugin for your editor.
They essentially show the output of a linter (such as this) inside your editor.

### `efm-langserver`

You can use the following config with [efm-langserver](https://github.com/mattn/efm-langserver):

```yaml
tools:
  gccdiag_cpp: &gccdiag_cpp
    lint-command: gccdiag --add-args='-S -x c++'
    lint-source: gccdiag
    lint-formats:
      - "%f:%l:%c: %trror: %m"
      - "%f:%l:%c: %tarning: %m"
      - "%f:%l:%c: %tote: %m"

  gccdiag_c: &gccdiag_c
    lint-command: gccdiag --add-args='-S -x c'
    lint-source: gccdiag
    lint-formats:
      - "%f:%l:%c: %trror: %m"
      - "%f:%l:%c: %tarning: %m"
      - "%f:%l:%c: %tote: %m"

languages:
  cpp:
    - <<: *gccdiag_cpp

  c:
    - <<: *gccdiag_c
```

Be sure to enable `efm-langserver` for `c` and `cpp` languages in your editor.

### `diagnostic-languageserver`

The following config works for [`diagnostic-langserver`](https://github.com/iamcco/diagnostic-languageserver):

##### `linters` field:

```json
{
  "gccdiag_cpp": {
    "command": "gccdiag",
    "debounce": 100,
    "args": ["--default-args", "-S -x c++", "--", "%file"],
    "isStdout": false,
    "isStderr": true,
    "offsetLine": 0,
    "offsetColumn": 0,
    "sourceName": "gccdiag",
    "formatLines": 1,
    "formatPattern": [
      "^[^:]+:(\\d+):(\\d+):\\s+([^:]+):\\s+(.*)$",
      {
        "line": 1,
        "column": 2,
        "message": 4,
        "security": 3
      }
    ],
    "securities": {
      "error": "error",
      "warning": "warning",
      "note": "info"
    }
  },
  "gccdiag_c": {
    "command": "gccdiag",
    "debounce": 100,
    "args": ["--default-args", "-S -x c", "--", "%file"],
    "isStdout": false,
    "isStderr": true,
    "offsetLine": 0,
    "offsetColumn": 0,
    "sourceName": "gccdiag",
    "formatLines": 1,
    "formatPattern": [
      "^[^:]+:(\\d+):(\\d+):\\s+([^:]+):\\s+(.*)$",
      {
        "line": 1,
        "column": 2,
        "message": 4,
        "security": 3
      }
    ],
    "securities": {
      "error": "error",
      "warning": "warning",
      "note": "info"
    }
  }
}
```

##### `filetypes` field

```json
"diagnostic-languageserver.filetypes": {
    "cpp": ["gccdiag_cpp"],
    "c": ["gccdiag_c"]
}
```

## Tips

You can use the `--compiler` argument to set a different `gcc` version when
you're working on a cross-platform or embedded project.
This allows you to get proper diagnostic messages from `arm-none-eabi-g++` or
`avr-gcc` or other similar compiler variants, or switch to regular `gcc`
(because it's faster or a newer version with better warnings).
By default, it uses the compiler specified in `compile_commands.json`.

## Contributing

I think that the base functionality of this little utility is complete - no
reason to add bloat to it.
Only if you have some really compelling, simple addition, I may reconsider.

If you find any bugs, please report them.

I haven't tested if this utility compiles or runs on Windows, but if someone
wants to test it and contribute the eventual fixes, I'm up for it.
